var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require("gulp-concat");

gulp.task('default', function() {
	console.log("gulp default task");
});

gulp.task('sass', function () {
  gulp.src('app/style/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('dist/stylecss'));
});

gulp.task('scripts', function() {
  return gulp.src('app/js/**/*.js')
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist/'));
});